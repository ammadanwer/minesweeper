// OptionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Minesweeper.h"
#include "OptionDlg.h"
#include "MinesweeperDlg.h"
#include"intermediate.h"
#include "ExitDlg.h"


// OptionDlg dialog

IMPLEMENT_DYNAMIC(OptionDlg, CDialog)

OptionDlg::OptionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(OptionDlg::IDD, pParent)
{
 
}

OptionDlg::~OptionDlg()
{
}

void OptionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BUTTON5, b100);
}


BEGIN_MESSAGE_MAP(OptionDlg, CDialog)
	
	ON_BN_CLICKED(IDC_BUTTON5, &OptionDlg::OnBnClickedButton5)
END_MESSAGE_MAP()


// OptionDlg message handlers

void OptionDlg::OnBnClickedButton5()
{	
	
	intermediate help;
	help.DoModal();
	OptionDlg::EndDialog(1);
	
	
}

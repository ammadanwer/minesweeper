// GameWonDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Minesweeper.h"
#include "GameWonDlg.h"


// GameWonDlg dialog

IMPLEMENT_DYNAMIC(GameWonDlg, CDialog)

GameWonDlg::GameWonDlg(CWnd* pParent /*=NULL*/)
	: CDialog(GameWonDlg::IDD, pParent)
{
	
		
}

GameWonDlg::~GameWonDlg()
{
}

void GameWonDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	
	DDX_Control(pDX, IDC_BUTTON2, b82);

}
BOOL GameWonDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	b82.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP2)) );
	return true;
}

BEGIN_MESSAGE_MAP(GameWonDlg, CDialog)

END_MESSAGE_MAP()


// GameWonDlg message handlers


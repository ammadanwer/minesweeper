// MinesweeperDlg.h : header file
//

#pragma once
#include "afxwin.h"



// CMinesweeperDlg dialog
class CMinesweeperDlg : public CDialog
{
// Construction
public:
	CMinesweeperDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MINESWEEPER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	void OnContextMenu(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

	CButton b1;
	
	afx_msg void OnGameExit();
	CButton b2;
		
	afx_msg void OnBnClickedButton84();
	afx_msg void OnGameNewgame();
	afx_msg void OnBnClickedButton70();
	CButton b3;
	CButton b4;
	CButton b5;
	CButton b6;
	CButton b7;
	CButton b8;
	afx_msg void OnBnClickedButton17();
	CButton b9;
	afx_msg void OnBnClickedButton71();
	afx_msg void OnBnClickedButton52();
	afx_msg void OnBnClickedButton53();
	afx_msg void OnBnClickedButton34();
	afx_msg void OnBnClickedButton35();
	afx_msg void OnBnClickedButton16();
	CButton b10;
	CButton b11;
	CButton b12;
	CButton b13;
	CButton b14;
	CButton b15;
	CButton b16;
	CButton b17;
	afx_msg void OnBnClickedButton83();
	afx_msg void OnBnClickedButton68();
	afx_msg void OnBnClickedButton69();
	afx_msg void OnBnClickedButton50();
	afx_msg void OnBnClickedButton51();
	afx_msg void OnBnClickedButton32();
	afx_msg void OnBnClickedButton33();
	afx_msg void OnBnClickedButton14();
	afx_msg void OnBnClickedButton15();
	CButton b18;
	CButton b19;
	afx_msg void OnBnClickedButton82();
	CButton b20;
	afx_msg void OnBnClickedButton66();
	CButton b21;
	afx_msg void OnBnClickedButton67();
	CButton b22;
	afx_msg void OnBnClickedButton48();
	CButton b23;
	afx_msg void OnBnClickedButton49();
	CButton b24;
	afx_msg void OnBnClickedButton30();
	CButton b25;
	afx_msg void OnBnClickedButton31();
	CButton b26;
	afx_msg void OnBnClickedButton12();
	CButton b27;
	afx_msg void OnBnClickedButton13();
	CButton b28;
	afx_msg void OnBnClickedButton81();
	CButton b29;
	afx_msg void OnBnClickedButton64();
	CButton b30;
	afx_msg void OnBnClickedButton65();
	CButton b31;
	afx_msg void OnBnClickedButton46();
	CButton b32;
	afx_msg void OnBnClickedButton47();
	CButton b33;
	afx_msg void OnBnClickedButton28();
	CButton b34;
	afx_msg void OnBnClickedButton29();
	CButton b35;
	afx_msg void OnBnClickedButton10();
	CButton b36;
	afx_msg void OnBnClickedButton11();
	CButton b37;
	afx_msg void OnBnClickedButton89();
	CButton b38;
	afx_msg void OnBnClickedButton80();
	CButton b39;
	afx_msg void OnBnClickedButton63();
	CButton b40;
	afx_msg void OnBnClickedButton62();
	CButton b41;
	afx_msg void OnBnClickedButton45();
	CButton b42;
	afx_msg void OnBnClickedButton44();
	CButton b43;
	afx_msg void OnBnClickedButton27();
	CButton b44;
	afx_msg void OnBnClickedButton26();
	CButton b45;
	afx_msg void OnBnClickedButton9();
	CButton b46;
	afx_msg void OnBnClickedButton88();
	CButton b47;
	afx_msg void OnBnClickedButton78();
	CButton b48;
	afx_msg void OnBnClickedButton79();
	CButton b49;
	afx_msg void OnBnClickedButton60();
	CButton b50;
	afx_msg void OnBnClickedButton61();
	CButton b51;
	afx_msg void OnBnClickedButton42();
	CButton b52;
	afx_msg void OnBnClickedButton43();
	CButton b53;
	afx_msg void OnBnClickedButton24();
	CButton b54;
	afx_msg void OnBnClickedButton25();
	CButton b55;
	afx_msg void OnBnClickedButton87();
	CButton b56;
	afx_msg void OnBnClickedButton76();
	CButton b57;
	afx_msg void OnBnClickedButton77();
	CButton b58;
	afx_msg void OnBnClickedButton58();
	CButton b59;
	afx_msg void OnBnClickedButton59();
	CButton b60;
	afx_msg void OnBnClickedButton40();
	CButton b61;
	afx_msg void OnBnClickedButton41();
	CButton b62;
	afx_msg void OnBnClickedButton22();
	CButton b63;
	afx_msg void OnBnClickedButton23();
	CButton b64;
	afx_msg void OnBnClickedButton86();
	CButton b65;
	afx_msg void OnBnClickedButton74();
	CButton b66;
	afx_msg void OnBnClickedButton75();
	CButton b67;
	afx_msg void OnBnClickedButton56();
	CButton b68;
	afx_msg void OnBnClickedButton57();
	CButton b69;
	afx_msg void OnBnClickedButton38();
	CButton b70;
	afx_msg void OnBnClickedButton39();
	CButton b71;
	afx_msg void OnBnClickedButton20();
	CButton b72;
	afx_msg void OnBnClickedButton21();
	CButton b73;
	afx_msg void OnBnClickedButton85();
	CButton b74;
	afx_msg void OnBnClickedButton72();
	CButton b75;
	afx_msg void OnBnClickedButton73();
	CButton b76;
	afx_msg void OnBnClickedButton54();
	CButton b77;
	afx_msg void OnBnClickedButton55();
	CButton b78;
	afx_msg void OnBnClickedButton36();
	CButton b79;
	afx_msg void OnBnClickedButton37();
	CButton b80;
	afx_msg void OnBnClickedButton18();
	CButton b81;
	afx_msg void OnBnClickedButton19();

	CButton b83;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnDoubleclickedButton84();
	afx_msg void OnGameChangeappearence();
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedCancel();
	

};

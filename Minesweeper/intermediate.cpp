// intermediate.cpp : implementation file
//

#include "stdafx.h"
#include "Minesweeper.h"
#include "intermediate.h"
#include "GameWonDlg.h"
#include <ctime>

int counts=0;
char Mines[16][16];
int chckifopen[257]={0};
int counter[257]={0};
// intermediate dialog
char detectmine(int x,int y)
{
	int numofmines=0;
	char n=' ';
	if(Mines[x][y]=='M')
	{
		
		return 'n';
	}
	else
	{
		if(Mines[x-1][y-1]=='M'&& x>=1 &&y>=1)
			numofmines++;
		if(Mines[x-1][y]=='M'&& x>=1)
			numofmines++;
		if(Mines[x-1][y+1]=='M'&& x>=1 &&y<=7)
			numofmines++;
		if(Mines[x][y-1]=='M'&& x>=0 &&y>=1)
			numofmines++;
		if(Mines[x][y+1]=='M'&& x>=0 &&y<=7)
			numofmines++;
		if(Mines[x+1][y-1]=='M'&& x<=7 &&y>=1)
			numofmines++;
		if(Mines[x+1][y]=='M'&& x<=7 &&y>=0)
			numofmines++;
		if(Mines[x+1][y+1]=='M'&& x<=7 &&y<=7)
			numofmines++;

		if(numofmines==0)
			return  n;
		else
		return (numofmines)+'0';
	}
}

IMPLEMENT_DYNAMIC(intermediate, CDialog)

intermediate::intermediate(CWnd* pParent /*=NULL*/)
	: CDialog(intermediate::IDD, pParent)
{
m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

intermediate::~intermediate()
{
}

void intermediate::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON246, bt1);
	DDX_Control(pDX, IDC_BUTTON232, bt2);
	DDX_Control(pDX, IDC_BUTTON233, bt3);
	DDX_Control(pDX, IDC_BUTTON214, bt4);
	DDX_Control(pDX, IDC_BUTTON215, bt5);
	DDX_Control(pDX, IDC_BUTTON196, bt6);
	DDX_Control(pDX, IDC_BUTTON197, bt7);
	DDX_Control(pDX, IDC_BUTTON178, bt8);
	DDX_Control(pDX, IDC_BUTTON179, bt9);
	DDX_Control(pDX, IDC_BUTTON165, bt10);
	DDX_Control(pDX, IDC_BUTTON151, bt11);
	DDX_Control(pDX, IDC_BUTTON152, bt12);
	DDX_Control(pDX, IDC_BUTTON133, bt13);
	DDX_Control(pDX, IDC_BUTTON134, bt14);
	DDX_Control(pDX, IDC_BUTTON115, bt15);
	DDX_Control(pDX, IDC_BUTTON116, bt16);
	DDX_Control(pDX, IDC_BUTTON245, bt17);
	DDX_Control(pDX, IDC_BUTTON230, bt18);
	DDX_Control(pDX, IDC_BUTTON231, bt19);
	DDX_Control(pDX, IDC_BUTTON212, bt20);
	DDX_Control(pDX, IDC_BUTTON213, bt21);
	DDX_Control(pDX, IDC_BUTTON194, bt22);
	DDX_Control(pDX, IDC_BUTTON195, bt23);
	DDX_Control(pDX, IDC_BUTTON176, bt24);
	DDX_Control(pDX, IDC_BUTTON177, bt25);
	DDX_Control(pDX, IDC_BUTTON164, bt26);
	DDX_Control(pDX, IDC_BUTTON149, bt27);
	DDX_Control(pDX, IDC_BUTTON150, bt28);
	DDX_Control(pDX, IDC_BUTTON131, bt29);
	DDX_Control(pDX, IDC_BUTTON132, bt30);
	DDX_Control(pDX, IDC_BUTTON113, bt31);
	DDX_Control(pDX, IDC_BUTTON114, bt32);
	DDX_Control(pDX, IDC_BUTTON244, bt33);
	DDX_Control(pDX, IDC_BUTTON228, bt34);
	DDX_Control(pDX, IDC_BUTTON229, bt35);
	DDX_Control(pDX, IDC_BUTTON210, bt36);
	DDX_Control(pDX, IDC_BUTTON211, bt37);
	DDX_Control(pDX, IDC_BUTTON192, bt38);
	DDX_Control(pDX, IDC_BUTTON193, bt39);
	DDX_Control(pDX, IDC_BUTTON174, bt40);
	DDX_Control(pDX, IDC_BUTTON175, bt41);
	DDX_Control(pDX, IDC_BUTTON163, bt42);
	DDX_Control(pDX, IDC_BUTTON147, bt43);
	DDX_Control(pDX, IDC_BUTTON148, bt44);
	DDX_Control(pDX, IDC_BUTTON129, bt45);
	DDX_Control(pDX, IDC_BUTTON130, bt46);
	DDX_Control(pDX, IDC_BUTTON111, bt47);
	DDX_Control(pDX, IDC_BUTTON112, bt48);
	DDX_Control(pDX, IDC_BUTTON243, bt49);
	DDX_Control(pDX, IDC_BUTTON226, bt50);
	DDX_Control(pDX, IDC_BUTTON227, bt51);
	DDX_Control(pDX, IDC_BUTTON208, bt52);
	DDX_Control(pDX, IDC_BUTTON209, bt53);
	DDX_Control(pDX, IDC_BUTTON190, bt54);
	DDX_Control(pDX, IDC_BUTTON191, bt55);
	DDX_Control(pDX, IDC_BUTTON172, bt56);
	DDX_Control(pDX, IDC_BUTTON173, bt57);
	DDX_Control(pDX, IDC_BUTTON162, bt58);
	DDX_Control(pDX, IDC_BUTTON145, bt59);
	DDX_Control(pDX, IDC_BUTTON146, bt60);
	DDX_Control(pDX, IDC_BUTTON127, bt61);
	DDX_Control(pDX, IDC_BUTTON128, bt62);
	DDX_Control(pDX, IDC_BUTTON109, bt63);
	DDX_Control(pDX, IDC_BUTTON110, bt64);
	DDX_Control(pDX, IDC_BUTTON251, bt65);
	DDX_Control(pDX, IDC_BUTTON242, bt66);
	DDX_Control(pDX, IDC_BUTTON225, bt67);
	DDX_Control(pDX, IDC_BUTTON224, bt68);
	DDX_Control(pDX, IDC_BUTTON207, bt69);
	DDX_Control(pDX, IDC_BUTTON206, bt70);
	DDX_Control(pDX, IDC_BUTTON189, bt71);
	DDX_Control(pDX, IDC_BUTTON188, bt72);
	DDX_Control(pDX, IDC_BUTTON171, bt73);
	DDX_Control(pDX, IDC_BUTTON170, bt74);
	DDX_Control(pDX, IDC_BUTTON161, bt75);
	DDX_Control(pDX, IDC_BUTTON144, bt76);
	DDX_Control(pDX, IDC_BUTTON143, bt77);
	DDX_Control(pDX, IDC_BUTTON126, bt78);
	DDX_Control(pDX, IDC_BUTTON125, bt79);
	DDX_Control(pDX, IDC_BUTTON108, bt80);
	DDX_Control(pDX, IDC_BUTTON250, bt81);
	DDX_Control(pDX, IDC_BUTTON240, bt82);
	DDX_Control(pDX, IDC_BUTTON241, bt83);
	DDX_Control(pDX, IDC_BUTTON222, bt84);
	DDX_Control(pDX, IDC_BUTTON223, bt85);
	DDX_Control(pDX, IDC_BUTTON204, bt86);
	DDX_Control(pDX, IDC_BUTTON205, bt87);
	DDX_Control(pDX, IDC_BUTTON186, bt88);
	DDX_Control(pDX, IDC_BUTTON187, bt89);
	DDX_Control(pDX, IDC_BUTTON169, bt90);
	DDX_Control(pDX, IDC_BUTTON159, bt91);
	DDX_Control(pDX, IDC_BUTTON160, bt92);
	DDX_Control(pDX, IDC_BUTTON141, bt93);
	DDX_Control(pDX, IDC_BUTTON142, bt94);
	DDX_Control(pDX, IDC_BUTTON123, bt95);
	DDX_Control(pDX, IDC_BUTTON124, bt96);
	DDX_Control(pDX, IDC_BUTTON249, bt97);
	DDX_Control(pDX, IDC_BUTTON238, bt98);
	DDX_Control(pDX, IDC_BUTTON239, bt99);
	DDX_Control(pDX, IDC_BUTTON220, bt100);
	DDX_Control(pDX, IDC_BUTTON221, bt101);
	DDX_Control(pDX, IDC_BUTTON202, bt102);
	DDX_Control(pDX, IDC_BUTTON203, bt103);
	DDX_Control(pDX, IDC_BUTTON184, bt104);
	DDX_Control(pDX, IDC_BUTTON185, bt105);
	DDX_Control(pDX, IDC_BUTTON168, bt106);
	DDX_Control(pDX, IDC_BUTTON157, bt107);
	DDX_Control(pDX, IDC_BUTTON158, bt108);
	DDX_Control(pDX, IDC_BUTTON139, bt109);
	DDX_Control(pDX, IDC_BUTTON140, bt110);
	DDX_Control(pDX, IDC_BUTTON121, bt111);
	DDX_Control(pDX, IDC_BUTTON122, bt112);
	DDX_Control(pDX, IDC_BUTTON248, bt113);
	DDX_Control(pDX, IDC_BUTTON236, bt114);
	DDX_Control(pDX, IDC_BUTTON237, bt115);
	DDX_Control(pDX, IDC_BUTTON218, bt116);
	DDX_Control(pDX, IDC_BUTTON219, bt117);
	DDX_Control(pDX, IDC_BUTTON200, bt118);
	DDX_Control(pDX, IDC_BUTTON201, bt119);
	DDX_Control(pDX, IDC_BUTTON182, bt120);
	DDX_Control(pDX, IDC_BUTTON183, bt121);
	DDX_Control(pDX, IDC_BUTTON167, bt122);
	DDX_Control(pDX, IDC_BUTTON155, bt123);
	DDX_Control(pDX, IDC_BUTTON156, bt124);
	DDX_Control(pDX, IDC_BUTTON137, bt125);
	DDX_Control(pDX, IDC_BUTTON138, bt126);
	DDX_Control(pDX, IDC_BUTTON119, bt127);
	DDX_Control(pDX, IDC_BUTTON120, bt128);
	DDX_Control(pDX, IDC_BUTTON247, bt129);
	DDX_Control(pDX, IDC_BUTTON234, bt130);
	DDX_Control(pDX, IDC_BUTTON235, bt131);
	DDX_Control(pDX, IDC_BUTTON216, bt132);
	DDX_Control(pDX, IDC_BUTTON217, bt133);
	DDX_Control(pDX, IDC_BUTTON198, bt134);
	DDX_Control(pDX, IDC_BUTTON199, bt135);
	DDX_Control(pDX, IDC_BUTTON180, bt136);
	DDX_Control(pDX, IDC_BUTTON181, bt137);
	DDX_Control(pDX, IDC_BUTTON166, bt138);
	DDX_Control(pDX, IDC_BUTTON153, bt139);
	DDX_Control(pDX, IDC_BUTTON154, bt140);
	DDX_Control(pDX, IDC_BUTTON135, bt141);
	DDX_Control(pDX, IDC_BUTTON136, bt142);
	DDX_Control(pDX, IDC_BUTTON117, bt143);
	DDX_Control(pDX, IDC_BUTTON118, bt144);
	DDX_Control(pDX, IDC_BUTTON390, bt145);
	DDX_Control(pDX, IDC_BUTTON376, bt146);
	DDX_Control(pDX, IDC_BUTTON377, bt147);
	DDX_Control(pDX, IDC_BUTTON358, bt148);
	DDX_Control(pDX, IDC_BUTTON359, bt149);
	DDX_Control(pDX, IDC_BUTTON340, bt150);
	DDX_Control(pDX, IDC_BUTTON341, bt151);
	DDX_Control(pDX, IDC_BUTTON322, bt152);
	DDX_Control(pDX, IDC_BUTTON323, bt153);
	DDX_Control(pDX, IDC_BUTTON309, bt154);
	DDX_Control(pDX, IDC_BUTTON295, bt155);
	DDX_Control(pDX, IDC_BUTTON296, bt156);
	DDX_Control(pDX, IDC_BUTTON277, bt157);
	DDX_Control(pDX, IDC_BUTTON278, bt158);
	DDX_Control(pDX, IDC_BUTTON259, bt159);
	DDX_Control(pDX, IDC_BUTTON260, bt160);
	DDX_Control(pDX, IDC_BUTTON389, bt161);
	DDX_Control(pDX, IDC_BUTTON374, bt162);
	DDX_Control(pDX, IDC_BUTTON375, bt163);
	DDX_Control(pDX, IDC_BUTTON356, bt164);
	DDX_Control(pDX, IDC_BUTTON357, bt165);
	DDX_Control(pDX, IDC_BUTTON338, bt166);
	DDX_Control(pDX, IDC_BUTTON339, bt167);
	DDX_Control(pDX, IDC_BUTTON320, bt168);
	DDX_Control(pDX, IDC_BUTTON321, bt169);
	DDX_Control(pDX, IDC_BUTTON308, bt170);
	DDX_Control(pDX, IDC_BUTTON293, bt171);
	DDX_Control(pDX, IDC_BUTTON294, bt172);
	DDX_Control(pDX, IDC_BUTTON275, bt173);
	DDX_Control(pDX, IDC_BUTTON276, bt174);
	DDX_Control(pDX, IDC_BUTTON257, bt175);
	DDX_Control(pDX, IDC_BUTTON258, bt176);
	DDX_Control(pDX, IDC_BUTTON388, bt177);
	DDX_Control(pDX, IDC_BUTTON372, bt178);
	DDX_Control(pDX, IDC_BUTTON373, bt179);
	DDX_Control(pDX, IDC_BUTTON354, bt180);
	DDX_Control(pDX, IDC_BUTTON355, bt181);
	DDX_Control(pDX, IDC_BUTTON336, bt182);
	DDX_Control(pDX, IDC_BUTTON337, bt183);
	DDX_Control(pDX, IDC_BUTTON318, bt184);
	DDX_Control(pDX, IDC_BUTTON319, bt185);
	DDX_Control(pDX, IDC_BUTTON307, bt186);
	DDX_Control(pDX, IDC_BUTTON291, bt187);
	DDX_Control(pDX, IDC_BUTTON292, bt188);
	DDX_Control(pDX, IDC_BUTTON273, bt189);
	DDX_Control(pDX, IDC_BUTTON274, bt190);
	DDX_Control(pDX, IDC_BUTTON255, bt191);
	DDX_Control(pDX, IDC_BUTTON256, bt192);
	DDX_Control(pDX, IDC_BUTTON387, bt193);
	DDX_Control(pDX, IDC_BUTTON370, bt194);
	DDX_Control(pDX, IDC_BUTTON371, bt195);
	DDX_Control(pDX, IDC_BUTTON352, bt196);
	DDX_Control(pDX, IDC_BUTTON353, bt197);
	DDX_Control(pDX, IDC_BUTTON334, bt198);
	DDX_Control(pDX, IDC_BUTTON335, bt199);
	DDX_Control(pDX, IDC_BUTTON316, bt200);
	DDX_Control(pDX, IDC_BUTTON317, bt201);
	DDX_Control(pDX, IDC_BUTTON306, bt202);
	DDX_Control(pDX, IDC_BUTTON289, bt203);
	DDX_Control(pDX, IDC_BUTTON290, bt204);
	DDX_Control(pDX, IDC_BUTTON271, bt205);
	DDX_Control(pDX, IDC_BUTTON272, bt206);
	DDX_Control(pDX, IDC_BUTTON253, bt207);
	DDX_Control(pDX, IDC_BUTTON254, bt208);
	DDX_Control(pDX, IDC_BUTTON395, bt209);
	DDX_Control(pDX, IDC_BUTTON386, bt210);
	DDX_Control(pDX, IDC_BUTTON369, bt211);
	DDX_Control(pDX, IDC_BUTTON368, bt212);
	DDX_Control(pDX, IDC_BUTTON351, bt213);
	DDX_Control(pDX, IDC_BUTTON350, bt214);
	DDX_Control(pDX, IDC_BUTTON333, bt215);
	DDX_Control(pDX, IDC_BUTTON332, bt216);
	DDX_Control(pDX, IDC_BUTTON315, bt217);
	DDX_Control(pDX, IDC_BUTTON314, bt218);
	DDX_Control(pDX, IDC_BUTTON305, bt219);
	DDX_Control(pDX, IDC_BUTTON288, bt220);
	DDX_Control(pDX, IDC_BUTTON287, bt221);
	DDX_Control(pDX, IDC_BUTTON270, bt222);
	DDX_Control(pDX, IDC_BUTTON269, bt223);
	DDX_Control(pDX, IDC_BUTTON252, bt224);
	DDX_Control(pDX, IDC_BUTTON394, bt225);
	DDX_Control(pDX, IDC_BUTTON384, bt226);
	DDX_Control(pDX, IDC_BUTTON385, bt227);
	DDX_Control(pDX, IDC_BUTTON366, bt228);
	DDX_Control(pDX, IDC_BUTTON367, bt229);
	DDX_Control(pDX, IDC_BUTTON348, bt230);
	DDX_Control(pDX, IDC_BUTTON349, bt231);
	DDX_Control(pDX, IDC_BUTTON330, bt232);
	DDX_Control(pDX, IDC_BUTTON331, bt233);
	DDX_Control(pDX, IDC_BUTTON313, bt234);
	DDX_Control(pDX, IDC_BUTTON303, bt235);
	DDX_Control(pDX, IDC_BUTTON304, bt236);
	DDX_Control(pDX, IDC_BUTTON285, bt237);
	DDX_Control(pDX, IDC_BUTTON286, bt238);
	DDX_Control(pDX, IDC_BUTTON267, bt239);
	DDX_Control(pDX, IDC_BUTTON268, bt240);
	DDX_Control(pDX, IDC_BUTTON393, bt241);
	DDX_Control(pDX, IDC_BUTTON382, bt242);
	DDX_Control(pDX, IDC_BUTTON383, bt243);
	DDX_Control(pDX, IDC_BUTTON364, bt244);
	DDX_Control(pDX, IDC_BUTTON365, bt245);
	DDX_Control(pDX, IDC_BUTTON346, bt246);
	DDX_Control(pDX, IDC_BUTTON347, bt247);
	DDX_Control(pDX, IDC_BUTTON328, bt248);
	DDX_Control(pDX, IDC_BUTTON329, bt249);
	DDX_Control(pDX, IDC_BUTTON312, bt250);
	DDX_Control(pDX, IDC_BUTTON301, bt251);
	DDX_Control(pDX, IDC_BUTTON302, bt252);
	DDX_Control(pDX, IDC_BUTTON283, bt253);
	DDX_Control(pDX, IDC_BUTTON284, bt254);
	DDX_Control(pDX, IDC_BUTTON265, bt255);
	DDX_Control(pDX, IDC_BUTTON266, bt256);
}


BEGIN_MESSAGE_MAP(intermediate, CDialog)

	ON_BN_CLICKED(IDC_BUTTON246, &intermediate::OnBnClickedButton246)
	ON_BN_CLICKED(IDC_BUTTON232, &intermediate::OnBnClickedButton232)
	ON_BN_CLICKED(IDC_BUTTON230, &intermediate::OnBnClickedButton230)
	ON_BN_CLICKED(IDC_BUTTON245, &intermediate::OnBnClickedButton245)
END_MESSAGE_MAP()

BOOL intermediate::OnInitDialog()
{
	srand(time(0));
			 int x=rand()%16;
			 int y=rand()%16;
			Mines[x][y]='M';

			 x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';


			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

			x=rand()%16;
			 y=rand()%16;
			Mines[x][y]='M';

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	
	// TODO: Add extra initialization here
	
	return TRUE;
}
// intermediate message handlers

void intermediate::OnBnClickedButton246()
{
	char x=detectmine(0,0);
	if(x!='n')
	{
		if(x==' ')
		{
			if(chckifopen[1]==0)
			{
				chckifopen[1]=1;
				OnBnClickedButton232();
				OnBnClickedButton230();
				OnBnClickedButton245();

			}
		}
		else
		{
			CString pp(x,1);		
			bt1.SetWindowTextW(pp);
		}
	
	}
	else
	{
		bt1.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	bt1.EnableWindow(0);if(counter[1]==0){counter[1]=1;counts++;};if(counts==71){GameWonDlg kk; kk.DoModal();}
}


void intermediate::OnBnClickedButton232()
{
	// TODO: Add your control notification handler code here
}

void intermediate::OnBnClickedButton230()
{
	// TODO: Add your control notification handler code here
}

void intermediate::OnBnClickedButton245()
{
	// TODO: Add your control notification handler code here
}

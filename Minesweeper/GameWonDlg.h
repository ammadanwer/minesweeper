#pragma once
#include "afxwin.h"


// GameWonDlg dialog

class GameWonDlg : public CDialog
{
	DECLARE_DYNAMIC(GameWonDlg)

public:
	GameWonDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~GameWonDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	
	CButton b82;
	afx_msg void OnBnClickedButton2();
};

// MinesweeperDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Minesweeper.h"
#include "MinesweeperDlg.h"
#include "ExitDlg.h"
#include "GameWonDlg.h"
#include "OptionDlg.h"
#include"intermediate.h"


#include<ctime>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif
int count=0;
char Mines[9][9];
int checkifopen[82]={0};
int forcount[82]={0};
char detectmines(int x,int y)
{
	int numofmines=0;
	char n=' ';
	if(Mines[x][y]=='M')
	{
		
		return 'n';
	}
	else
	{
		if(Mines[x-1][y-1]=='M'&& x>=1 &&y>=1)
			numofmines++;
		if(Mines[x-1][y]=='M'&& x>=1)
			numofmines++;
		if(Mines[x-1][y+1]=='M'&& x>=1 &&y<=7)
			numofmines++;
		if(Mines[x][y-1]=='M'&& x>=0 &&y>=1)
			numofmines++;
		if(Mines[x][y+1]=='M'&& x>=0 &&y<=7)
			numofmines++;
		if(Mines[x+1][y-1]=='M'&& x<=7 &&y>=1)
			numofmines++;
		if(Mines[x+1][y]=='M'&& x<=7 &&y>=0)
			numofmines++;
		if(Mines[x+1][y+1]=='M'&& x<=7 &&y<=7)
			numofmines++;

		if(numofmines==0)
			return  n;
		else
		return (numofmines)+'0';
	}
}

// CAboutDlg dialog used for App About
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}


void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMinesweeperDlg dialog




CMinesweeperDlg::CMinesweeperDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMinesweeperDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

void CMinesweeperDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BUTTON84, b1);
	DDX_Control(pDX, IDC_BUTTON70, b2);
	DDX_Control(pDX, IDC_BUTTON71, b3);
	DDX_Control(pDX, IDC_BUTTON52, b4);
	DDX_Control(pDX, IDC_BUTTON53, b5);
	DDX_Control(pDX, IDC_BUTTON34, b6);
	DDX_Control(pDX, IDC_BUTTON35, b7);
	DDX_Control(pDX, IDC_BUTTON16, b8);
	DDX_Control(pDX, IDC_BUTTON17, b9);
	DDX_Control(pDX, IDC_BUTTON83, b10);
	DDX_Control(pDX, IDC_BUTTON68, b11);
	DDX_Control(pDX, IDC_BUTTON69, b12);
	DDX_Control(pDX, IDC_BUTTON50, b13);
	DDX_Control(pDX, IDC_BUTTON51, b14);
	DDX_Control(pDX, IDC_BUTTON32, b15);
	DDX_Control(pDX, IDC_BUTTON33, b16);
	DDX_Control(pDX, IDC_BUTTON14, b17);
	DDX_Control(pDX, IDC_BUTTON15, b18);
	DDX_Control(pDX, IDC_BUTTON82, b19);
	DDX_Control(pDX, IDC_BUTTON66, b20);
	DDX_Control(pDX, IDC_BUTTON67, b21);
	DDX_Control(pDX, IDC_BUTTON48, b22);
	DDX_Control(pDX, IDC_BUTTON49, b23);
	DDX_Control(pDX, IDC_BUTTON30, b24);
	DDX_Control(pDX, IDC_BUTTON31, b25);
	DDX_Control(pDX, IDC_BUTTON12, b26);
	DDX_Control(pDX, IDC_BUTTON13, b27);
	DDX_Control(pDX, IDC_BUTTON81, b28);
	DDX_Control(pDX, IDC_BUTTON64, b29);
	DDX_Control(pDX, IDC_BUTTON65, b30);
	DDX_Control(pDX, IDC_BUTTON46, b31);
	DDX_Control(pDX, IDC_BUTTON47, b32);
	DDX_Control(pDX, IDC_BUTTON28, b33);
	DDX_Control(pDX, IDC_BUTTON29, b34);
	DDX_Control(pDX, IDC_BUTTON10, b35);
	DDX_Control(pDX, IDC_BUTTON11, b36);
	DDX_Control(pDX, IDC_BUTTON89, b37);
	DDX_Control(pDX, IDC_BUTTON80, b38);
	DDX_Control(pDX, IDC_BUTTON63, b39);
	DDX_Control(pDX, IDC_BUTTON62, b40);
	DDX_Control(pDX, IDC_BUTTON45, b41);
	DDX_Control(pDX, IDC_BUTTON44, b42);
	DDX_Control(pDX, IDC_BUTTON27, b43);
	DDX_Control(pDX, IDC_BUTTON26, b44);
	DDX_Control(pDX, IDC_BUTTON9, b45);
	DDX_Control(pDX, IDC_BUTTON88, b46);
	DDX_Control(pDX, IDC_BUTTON78, b47);
	DDX_Control(pDX, IDC_BUTTON79, b48);
	DDX_Control(pDX, IDC_BUTTON60, b49);
	DDX_Control(pDX, IDC_BUTTON61, b50);
	DDX_Control(pDX, IDC_BUTTON42, b51);
	DDX_Control(pDX, IDC_BUTTON43, b52);
	DDX_Control(pDX, IDC_BUTTON24, b53);
	DDX_Control(pDX, IDC_BUTTON25, b54);
	DDX_Control(pDX, IDC_BUTTON87, b55);
	DDX_Control(pDX, IDC_BUTTON76, b56);
	DDX_Control(pDX, IDC_BUTTON77, b57);
	DDX_Control(pDX, IDC_BUTTON58, b58);
	DDX_Control(pDX, IDC_BUTTON59, b59);
	DDX_Control(pDX, IDC_BUTTON40, b60);
	DDX_Control(pDX, IDC_BUTTON41, b61);
	DDX_Control(pDX, IDC_BUTTON22, b62);
	DDX_Control(pDX, IDC_BUTTON23, b63);
	DDX_Control(pDX, IDC_BUTTON86, b64);
	DDX_Control(pDX, IDC_BUTTON74, b65);
	DDX_Control(pDX, IDC_BUTTON75, b66);
	DDX_Control(pDX, IDC_BUTTON56, b67);
	DDX_Control(pDX, IDC_BUTTON57, b68);
	DDX_Control(pDX, IDC_BUTTON38, b69);
	DDX_Control(pDX, IDC_BUTTON39, b70);
	DDX_Control(pDX, IDC_BUTTON20, b71);
	DDX_Control(pDX, IDC_BUTTON21, b72);
	DDX_Control(pDX, IDC_BUTTON85, b73);
	DDX_Control(pDX, IDC_BUTTON72, b74);
	DDX_Control(pDX, IDC_BUTTON73, b75);
	DDX_Control(pDX, IDC_BUTTON54, b76);
	DDX_Control(pDX, IDC_BUTTON55, b77);
	DDX_Control(pDX, IDC_BUTTON36, b78);
	DDX_Control(pDX, IDC_BUTTON37, b79);
	DDX_Control(pDX, IDC_BUTTON18, b80);
	DDX_Control(pDX, IDC_BUTTON19, b81);
	DDX_Control(pDX, IDC_BUTTON1, b83);
}

BEGIN_MESSAGE_MAP(CMinesweeperDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP


	ON_COMMAND(ID_GAME_EXIT, &CMinesweeperDlg::OnGameExit)
	
	ON_BN_CLICKED(IDC_BUTTON84, &CMinesweeperDlg::OnBnClickedButton84)
	ON_COMMAND(ID_GAME_NEWGAME, &CMinesweeperDlg::OnGameNewgame)
	ON_BN_CLICKED(IDC_BUTTON70, &CMinesweeperDlg::OnBnClickedButton70)
	ON_BN_CLICKED(IDC_BUTTON17, &CMinesweeperDlg::OnBnClickedButton17)
	ON_BN_CLICKED(IDC_BUTTON71, &CMinesweeperDlg::OnBnClickedButton71)
	ON_BN_CLICKED(IDC_BUTTON52, &CMinesweeperDlg::OnBnClickedButton52)
	ON_BN_CLICKED(IDC_BUTTON53, &CMinesweeperDlg::OnBnClickedButton53)
	ON_BN_CLICKED(IDC_BUTTON34, &CMinesweeperDlg::OnBnClickedButton34)
	ON_BN_CLICKED(IDC_BUTTON35, &CMinesweeperDlg::OnBnClickedButton35)
	ON_BN_CLICKED(IDC_BUTTON16, &CMinesweeperDlg::OnBnClickedButton16)
	ON_BN_CLICKED(IDC_BUTTON83, &CMinesweeperDlg::OnBnClickedButton83)
	ON_BN_CLICKED(IDC_BUTTON68, &CMinesweeperDlg::OnBnClickedButton68)
	ON_BN_CLICKED(IDC_BUTTON69, &CMinesweeperDlg::OnBnClickedButton69)
	ON_BN_CLICKED(IDC_BUTTON50, &CMinesweeperDlg::OnBnClickedButton50)
	ON_BN_CLICKED(IDC_BUTTON51, &CMinesweeperDlg::OnBnClickedButton51)
	ON_BN_CLICKED(IDC_BUTTON32, &CMinesweeperDlg::OnBnClickedButton32)
	ON_BN_CLICKED(IDC_BUTTON33, &CMinesweeperDlg::OnBnClickedButton33)
	ON_BN_CLICKED(IDC_BUTTON14, &CMinesweeperDlg::OnBnClickedButton14)
	ON_BN_CLICKED(IDC_BUTTON15, &CMinesweeperDlg::OnBnClickedButton15)
	ON_BN_CLICKED(IDC_BUTTON82, &CMinesweeperDlg::OnBnClickedButton82)
	ON_BN_CLICKED(IDC_BUTTON66, &CMinesweeperDlg::OnBnClickedButton66)
	ON_BN_CLICKED(IDC_BUTTON67, &CMinesweeperDlg::OnBnClickedButton67)
	ON_BN_CLICKED(IDC_BUTTON48, &CMinesweeperDlg::OnBnClickedButton48)
	ON_BN_CLICKED(IDC_BUTTON49, &CMinesweeperDlg::OnBnClickedButton49)
	ON_BN_CLICKED(IDC_BUTTON30, &CMinesweeperDlg::OnBnClickedButton30)
	ON_BN_CLICKED(IDC_BUTTON31, &CMinesweeperDlg::OnBnClickedButton31)
	ON_BN_CLICKED(IDC_BUTTON12, &CMinesweeperDlg::OnBnClickedButton12)
	ON_BN_CLICKED(IDC_BUTTON13, &CMinesweeperDlg::OnBnClickedButton13)
	ON_BN_CLICKED(IDC_BUTTON81, &CMinesweeperDlg::OnBnClickedButton81)
	ON_BN_CLICKED(IDC_BUTTON64, &CMinesweeperDlg::OnBnClickedButton64)
	ON_BN_CLICKED(IDC_BUTTON65, &CMinesweeperDlg::OnBnClickedButton65)
	ON_BN_CLICKED(IDC_BUTTON46, &CMinesweeperDlg::OnBnClickedButton46)
	ON_BN_CLICKED(IDC_BUTTON47, &CMinesweeperDlg::OnBnClickedButton47)
	ON_BN_CLICKED(IDC_BUTTON28, &CMinesweeperDlg::OnBnClickedButton28)
	ON_BN_CLICKED(IDC_BUTTON29, &CMinesweeperDlg::OnBnClickedButton29)
	ON_BN_CLICKED(IDC_BUTTON10, &CMinesweeperDlg::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON11, &CMinesweeperDlg::OnBnClickedButton11)
	ON_BN_CLICKED(IDC_BUTTON89, &CMinesweeperDlg::OnBnClickedButton89)
	ON_BN_CLICKED(IDC_BUTTON80, &CMinesweeperDlg::OnBnClickedButton80)
	ON_BN_CLICKED(IDC_BUTTON63, &CMinesweeperDlg::OnBnClickedButton63)
	ON_BN_CLICKED(IDC_BUTTON62, &CMinesweeperDlg::OnBnClickedButton62)
	ON_BN_CLICKED(IDC_BUTTON45, &CMinesweeperDlg::OnBnClickedButton45)
	ON_BN_CLICKED(IDC_BUTTON44, &CMinesweeperDlg::OnBnClickedButton44)
	ON_BN_CLICKED(IDC_BUTTON27, &CMinesweeperDlg::OnBnClickedButton27)
	ON_BN_CLICKED(IDC_BUTTON26, &CMinesweeperDlg::OnBnClickedButton26)
	ON_BN_CLICKED(IDC_BUTTON9, &CMinesweeperDlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON88, &CMinesweeperDlg::OnBnClickedButton88)
	ON_BN_CLICKED(IDC_BUTTON78, &CMinesweeperDlg::OnBnClickedButton78)
	ON_BN_CLICKED(IDC_BUTTON79, &CMinesweeperDlg::OnBnClickedButton79)
	ON_BN_CLICKED(IDC_BUTTON60, &CMinesweeperDlg::OnBnClickedButton60)
	ON_BN_CLICKED(IDC_BUTTON61, &CMinesweeperDlg::OnBnClickedButton61)
	ON_BN_CLICKED(IDC_BUTTON42, &CMinesweeperDlg::OnBnClickedButton42)
	ON_BN_CLICKED(IDC_BUTTON43, &CMinesweeperDlg::OnBnClickedButton43)
	ON_BN_CLICKED(IDC_BUTTON24, &CMinesweeperDlg::OnBnClickedButton24)
	ON_BN_CLICKED(IDC_BUTTON25, &CMinesweeperDlg::OnBnClickedButton25)
	ON_BN_CLICKED(IDC_BUTTON87, &CMinesweeperDlg::OnBnClickedButton87)
	ON_BN_CLICKED(IDC_BUTTON76, &CMinesweeperDlg::OnBnClickedButton76)
	ON_BN_CLICKED(IDC_BUTTON77, &CMinesweeperDlg::OnBnClickedButton77)
	ON_BN_CLICKED(IDC_BUTTON58, &CMinesweeperDlg::OnBnClickedButton58)
	ON_BN_CLICKED(IDC_BUTTON59, &CMinesweeperDlg::OnBnClickedButton59)
	ON_BN_CLICKED(IDC_BUTTON40, &CMinesweeperDlg::OnBnClickedButton40)
	ON_BN_CLICKED(IDC_BUTTON41, &CMinesweeperDlg::OnBnClickedButton41)
	ON_BN_CLICKED(IDC_BUTTON22, &CMinesweeperDlg::OnBnClickedButton22)
	ON_BN_CLICKED(IDC_BUTTON23, &CMinesweeperDlg::OnBnClickedButton23)
	ON_BN_CLICKED(IDC_BUTTON86, &CMinesweeperDlg::OnBnClickedButton86)
	ON_BN_CLICKED(IDC_BUTTON74, &CMinesweeperDlg::OnBnClickedButton74)
	ON_BN_CLICKED(IDC_BUTTON75, &CMinesweeperDlg::OnBnClickedButton75)
	ON_BN_CLICKED(IDC_BUTTON56, &CMinesweeperDlg::OnBnClickedButton56)
	ON_BN_CLICKED(IDC_BUTTON57, &CMinesweeperDlg::OnBnClickedButton57)
	ON_BN_CLICKED(IDC_BUTTON38, &CMinesweeperDlg::OnBnClickedButton38)
	ON_BN_CLICKED(IDC_BUTTON39, &CMinesweeperDlg::OnBnClickedButton39)
	ON_BN_CLICKED(IDC_BUTTON20, &CMinesweeperDlg::OnBnClickedButton20)
	ON_BN_CLICKED(IDC_BUTTON21, &CMinesweeperDlg::OnBnClickedButton21)
	ON_BN_CLICKED(IDC_BUTTON85, &CMinesweeperDlg::OnBnClickedButton85)
	ON_BN_CLICKED(IDC_BUTTON72, &CMinesweeperDlg::OnBnClickedButton72)
	ON_BN_CLICKED(IDC_BUTTON73, &CMinesweeperDlg::OnBnClickedButton73)
	ON_BN_CLICKED(IDC_BUTTON54, &CMinesweeperDlg::OnBnClickedButton54)
	ON_BN_CLICKED(IDC_BUTTON55, &CMinesweeperDlg::OnBnClickedButton55)
	ON_BN_CLICKED(IDC_BUTTON36, &CMinesweeperDlg::OnBnClickedButton36)
	ON_BN_CLICKED(IDC_BUTTON37, &CMinesweeperDlg::OnBnClickedButton37)
	ON_BN_CLICKED(IDC_BUTTON18, &CMinesweeperDlg::OnBnClickedButton18)
	ON_BN_CLICKED(IDC_BUTTON19, &CMinesweeperDlg::OnBnClickedButton19)
	
	ON_BN_CLICKED(IDC_BUTTON1, &CMinesweeperDlg::OnBnClickedButton1)
	
	ON_COMMAND(ID_GAME_CHANGEAPPEARENCE, &CMinesweeperDlg::OnGameChangeappearence)
	
END_MESSAGE_MAP()


// CMinesweeperDlg message handlers

BOOL CMinesweeperDlg::OnInitDialog()
{	

	CDialog::OnInitDialog();
	
	b83.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP3)) );
	
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}
void CMinesweeperDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMinesweeperDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
		srand(time(0));
			int x=rand()%9;
			int y=rand()%9;
			Mines[x][y]='M';

			x=rand()%9;
			y=rand()%9;
			Mines[x][y]='M';

			x=rand()%9;
			y=rand()%9;
			Mines[x][y]='M';

			x=rand()%9;
			y=rand()%9;
			Mines[x][y]='M';

			x=rand()%9;
			y=rand()%9;
			Mines[x][y]='M';

			x=rand()%9;
			y=rand()%9;
			Mines[x][y]='M';

			x=rand()%9;
			y=rand()%9;
			Mines[x][y]='M';

			x=rand()%9;
			y=rand()%9;
			Mines[x][y]='M';

			x=rand()%9;
			y=rand()%9;
			Mines[x][y]='M';

			x=rand()%9;
			y=rand()%9;
			Mines[x][y]='M';


		
	
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMinesweeperDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMinesweeperDlg::OnGameExit()
{
	ExitDlg hello;
	int x=hello.DoModal();
	if (x!=1)
	{
		::PostQuitMessage(0);

	}
}


void CMinesweeperDlg::OnBnClickedButton84()
{ 
	char x=detectmines(0,0);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[1]==0)
			{
				checkifopen[1]=1;
				OnBnClickedButton70();
				OnBnClickedButton68();
				OnBnClickedButton83();

			}
		}
		else
		{
			CString pp(x,1);		
			b1.SetWindowTextW(pp);
		}
	
	}
	else
	{
		b1.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b1.EnableWindow(0);if(forcount[1]==0){forcount[1]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnGameNewgame()
{
	
}

void CMinesweeperDlg::OnBnClickedButton70()
{
	char x=detectmines(0,1);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[2]==0)
			{
				checkifopen[2]=1;
				OnBnClickedButton84();
				OnBnClickedButton71();
				OnBnClickedButton83();
				OnBnClickedButton68();
				OnBnClickedButton69();

			}
		}
		else
		{
			CString pp(x,1);		
			b2.SetWindowTextW(pp);
		}
	}
	else
	{
		b2.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b2.EnableWindow(0);if(forcount[2]==0){forcount[2]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton17()
{
	char x=detectmines(0,8);
	
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[9]==0)
			{
				checkifopen[9]=1;
				OnBnClickedButton16();
				OnBnClickedButton14();
				OnBnClickedButton15();
				
			}
		}

	else
		{
			CString pp(x,1);		
			b9.SetWindowTextW(pp);
		}
	}
	else
	{
		b9.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b9.EnableWindow(0);if(forcount[9]==0){forcount[9]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton71()
{
	char x=detectmines(0,2);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[3]==0)
			{
				checkifopen[3]=1;
				OnBnClickedButton52();
				OnBnClickedButton70();
				OnBnClickedButton50();
				OnBnClickedButton68();
				OnBnClickedButton69();

			}
		}
		else
		{
			CString pp(x,1);		
			b3.SetWindowTextW(pp);
		}
	}
	else
	{
		b3.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b3.EnableWindow(0);if(forcount[3]==0){forcount[3]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton52()
{
	char x=detectmines(0,3);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[4]==0)
			{
				checkifopen[4]=1;
				OnBnClickedButton53();
				OnBnClickedButton71();
				OnBnClickedButton50();
				OnBnClickedButton51();
				OnBnClickedButton69();

			}
		}
		else
		{
			CString pp(x,1);		
			b4.SetWindowTextW(pp);
		}
	}
	else
	{
		b4.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b4.EnableWindow(0);if(forcount[4]==0){forcount[4]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton53()
{
	char x=detectmines(0,4);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[5]==0)
			{
				checkifopen[5]=1;
				OnBnClickedButton52();
				OnBnClickedButton34();
				OnBnClickedButton50();
				OnBnClickedButton51();
				OnBnClickedButton32();

			}
		}
		else
		{
			CString pp(x,1);		
			b5.SetWindowTextW(pp);
		}
	}
	else
	{
		b5.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b5.EnableWindow(0);if(forcount[5]==0){forcount[5]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton34()
{
	char x=detectmines(0,5);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[6]==0)
			{
				checkifopen[6]=1;
				OnBnClickedButton53();
				OnBnClickedButton35();
				OnBnClickedButton51();
				OnBnClickedButton32();
				OnBnClickedButton33();

			}
		}
		else
		{
			CString pp(x,1);		
			b6.SetWindowTextW(pp);
		}
	}
	else
	{
		b6.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b6.EnableWindow(0);if(forcount[6]==0){forcount[6]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton35()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(0,6);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[7]==0)
			{
				checkifopen[7]=1;
				OnBnClickedButton34();
				OnBnClickedButton16();
				OnBnClickedButton32();
				OnBnClickedButton33();
				OnBnClickedButton34();

			}
		}
		else
		{
			CString pp(x,1);		
			b7.SetWindowTextW(pp);
		}
	}
	else
	{
		b7.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b7.EnableWindow(0);if(forcount[7]==0){forcount[7]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton16()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(0,7);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[8]==0)
			{
				checkifopen[8]=1;
				OnBnClickedButton35();
				OnBnClickedButton17();
				OnBnClickedButton33();
				OnBnClickedButton14();
				OnBnClickedButton15();

			}
		}
		else
		{
			CString pp(x,1);		
			b8.SetWindowTextW(pp);
		}
	}
	else
	{
		b8.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b8.EnableWindow(0);if(forcount[8]==0){forcount[8]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton83()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(1,0);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[10]==0)
			{
				checkifopen[10]=1;
				OnBnClickedButton84();
				OnBnClickedButton70();
				OnBnClickedButton68();
				OnBnClickedButton66();
				OnBnClickedButton82();

			}
		}
		else
	{
		CString pp(x,1);		
		b10.SetWindowTextW(pp);
	}
	}
	else
	{
		b10.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b10.EnableWindow(0);if(forcount[10]==0){forcount[10]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton68()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(1,1);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[11]==0)
			{
				checkifopen[11]=1;
				OnBnClickedButton84();
				OnBnClickedButton70();
				OnBnClickedButton69();
				OnBnClickedButton66();
				OnBnClickedButton82();
				OnBnClickedButton71();
				OnBnClickedButton83();
				OnBnClickedButton67();
			}
		}
		else
	{
		CString pp(x,1);		
		b11.SetWindowTextW(pp);
	}
	}
	else
	{
		b11.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b11.EnableWindow(0);if(forcount[11]==0){forcount[11]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton69()
{
	char x=detectmines(1,2);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[12]==0)
			{
				checkifopen[12]=1;
				OnBnClickedButton52();
				OnBnClickedButton70();
				OnBnClickedButton68();
				OnBnClickedButton66();
				OnBnClickedButton48();
				OnBnClickedButton71();
				OnBnClickedButton50();
				OnBnClickedButton67();
			}
		}
		else
	{
		CString pp(x,1);		
		b12.SetWindowTextW(pp);
	}
	}
	else
	{
		b12.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b12.EnableWindow(0);if(forcount[12]==0){forcount[12]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton50()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(1,3);
	if(x!='n')
	{
		if(x==' ')
		{
			if(checkifopen[13]==0)
			{
				checkifopen[13]=1;
				OnBnClickedButton71();
				OnBnClickedButton69();
				OnBnClickedButton48();
				OnBnClickedButton51();
				OnBnClickedButton67();
				OnBnClickedButton49();
				OnBnClickedButton53();
				OnBnClickedButton52();
			}
		}
		else
	{
		CString pp(x,1);		
		b13.SetWindowTextW(pp);
	}
	}
	else
	{
		b13.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b13.EnableWindow(0);if(forcount[13]==0){forcount[13]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton51()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(1,4);
	if(x!='n')
	
		{
		if(x==' ')
		{
			if(checkifopen[14]==0)
			{
				checkifopen[14]=1;
				OnBnClickedButton52();
				OnBnClickedButton49();
				OnBnClickedButton30();
				OnBnClickedButton32();
				OnBnClickedButton48();
				OnBnClickedButton34();
				OnBnClickedButton50();
				OnBnClickedButton53();
			}
		}
		else
		{
			CString pp(x,1);		
			b14.SetWindowTextW(pp);
		}
	}	
	else
	{
		b14.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b14.EnableWindow(0);if(forcount[14]==0){forcount[14]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton32()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(1,5);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[15]==0)
			{
				checkifopen[15]=1;
				OnBnClickedButton51();
				OnBnClickedButton30();
				OnBnClickedButton31();
				OnBnClickedButton33();
				OnBnClickedButton49();
				OnBnClickedButton34();
				OnBnClickedButton35();
				OnBnClickedButton53();
			}
		}
		else
	{
		CString pp(x,1);		
		b15.SetWindowTextW(pp);
	}
	}
	else
	{
		b15.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b15.EnableWindow(0);if(forcount[15]==0){forcount[15]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton33()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(1,6);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[16]==0)
			{
				checkifopen[16]=1;
				OnBnClickedButton34();
				OnBnClickedButton35();
				OnBnClickedButton16();
				OnBnClickedButton14();
				OnBnClickedButton12();
				OnBnClickedButton31();
				OnBnClickedButton30();
				OnBnClickedButton32();
			}
		}
		else
	{
		CString pp(x,1);		
		b16.SetWindowTextW(pp);

	}
	}
	else
	{
		b16.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b16.EnableWindow(0);if(forcount[16]==0){forcount[16]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton14()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(1,7);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[17]==0)
			{
				checkifopen[17]=1;
				OnBnClickedButton35();
				OnBnClickedButton16();
				OnBnClickedButton17();
				OnBnClickedButton15();
				OnBnClickedButton13();
				OnBnClickedButton12();
				OnBnClickedButton31();
				OnBnClickedButton33();
			}
		}
		else
	{
		CString pp(x,1);		
		b17.SetWindowTextW(pp);
	}
	}
	else
	{
		b17.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b17.EnableWindow(0);if(forcount[17]==0){forcount[17]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton15()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(1,8);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[18]==0)
			{
				checkifopen[18]=1;
				OnBnClickedButton16();
				OnBnClickedButton17();
				OnBnClickedButton14();
				OnBnClickedButton12();
				OnBnClickedButton13();
			}
		}
		else
	{
		CString pp(x,1);		
		b18.SetWindowTextW(pp);
	}
	}
	else
	{
		b18.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b18.EnableWindow(0);if(forcount[18]==0){forcount[18]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton82()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(2,0);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[19]==0)
			{
				checkifopen[19]=1;
				OnBnClickedButton83();
				OnBnClickedButton68();
				OnBnClickedButton66();
				OnBnClickedButton64();
				OnBnClickedButton81();
			}
		}
		else
	{
		CString pp(x,1);		
		b19.SetWindowTextW(pp);
	}
	}
	else
	{
		b19.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b19.EnableWindow(0);if(forcount[19]==0){forcount[19]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton66()
{
	// TODO: Add your control notification handler code here
	char x=detectmines(2,1);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[20]==0)
			{
				checkifopen[20]=1;
				OnBnClickedButton83();
				OnBnClickedButton68();
				OnBnClickedButton69();
				OnBnClickedButton67();
				OnBnClickedButton65();
				OnBnClickedButton64();
				OnBnClickedButton81();
				OnBnClickedButton82();
			}
		}
		else
	{
		CString pp(x,1);		
		b20.SetWindowTextW(pp);
	}
	}
	else
	{
		b20.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b20.EnableWindow(0);if(forcount[20]==0){forcount[20]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton67()
{
	char x=detectmines(2,2);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[21]==0)
			{
				checkifopen[21]=1;
				OnBnClickedButton68();
				OnBnClickedButton69();
				OnBnClickedButton50();
				OnBnClickedButton48();
				OnBnClickedButton46();
				OnBnClickedButton65();
				OnBnClickedButton64();
				OnBnClickedButton66();
			}
		}
		else
	{
		CString pp(x,1);		
		b21.SetWindowTextW(pp);
	}
	}
	else
	{
		b21.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b21.EnableWindow(0);if(forcount[21]==0){forcount[21]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton48()
{
	char x=detectmines(2,3);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[22]==0)
			{
				checkifopen[22]=1;
				OnBnClickedButton69();
				OnBnClickedButton50();
				OnBnClickedButton51();
				OnBnClickedButton49();
				OnBnClickedButton47();
				OnBnClickedButton46();
				OnBnClickedButton65();
				OnBnClickedButton67();
			}
		}
		else
	{
		CString pp(x,1);		
		b22.SetWindowTextW(pp);
	}
	}
	else
	{
		b22.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b22.EnableWindow(0);if(forcount[22]==0){forcount[22]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton49()
{
	char x=detectmines(2,4);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[23]==0)
			{
				checkifopen[23]=1;
				OnBnClickedButton50();
				OnBnClickedButton51();
				OnBnClickedButton32();
				OnBnClickedButton30();
				OnBnClickedButton28();
				OnBnClickedButton47();
				OnBnClickedButton46();
				OnBnClickedButton48();
			}
		}
		else
	{
		CString pp(x,1);		
		b23.SetWindowTextW(pp);
	}
	}
	else
	{
		b23.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b23.EnableWindow(0);if(forcount[23]==0){forcount[23]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton30()
{
	char x=detectmines(2,5);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[2]==0)
			{
				checkifopen[24]=1;
				OnBnClickedButton51();
				OnBnClickedButton32();
				OnBnClickedButton33();
				OnBnClickedButton31();
				OnBnClickedButton29();
				OnBnClickedButton28();
				OnBnClickedButton47();
				OnBnClickedButton49();
			}
		}
		else
	{
		CString pp(x,1);		
		b24.SetWindowTextW(pp);
	}
	}
	else
	{
		b24.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b24.EnableWindow(0);if(forcount[24]==0){forcount[24]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton31()
{
	char x=detectmines(2,6);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[25]==0)
			{
				checkifopen[25]=1;
				OnBnClickedButton32();
				OnBnClickedButton33();
				OnBnClickedButton14();
				OnBnClickedButton12();
				OnBnClickedButton10();
				OnBnClickedButton29();
				OnBnClickedButton28();
				OnBnClickedButton30();
			}
		}
		else
	{
		CString pp(x,1);		
		b25.SetWindowTextW(pp);
	}
	}
	else
	{
		b25.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b25.EnableWindow(0);if(forcount[25]==0){forcount[25]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton12()
{
	char x=detectmines(2,7);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[26]==0)
			{
				checkifopen[26]=1;
				OnBnClickedButton33();
				OnBnClickedButton14();
				OnBnClickedButton15();
				OnBnClickedButton13();
				OnBnClickedButton11();
				OnBnClickedButton10();
				OnBnClickedButton29();
				OnBnClickedButton31();
			}
		}
		else
	{
		CString pp(x,1);		
		b26.SetWindowTextW(pp);
	}
	}
	else
	{
		b26.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b26.EnableWindow(0);if(forcount[26]==0){forcount[26]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton13()
{
	char x=detectmines(2,8);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[27]==0)
			{
				checkifopen[27]=1;
				OnBnClickedButton14();
				OnBnClickedButton15();
				OnBnClickedButton12();
				OnBnClickedButton10();
				OnBnClickedButton11();
			}
		}
		else
	{
		CString pp(x,1);		
		b27.SetWindowTextW(pp);
	}
	}
	else
	{
		b27.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b27.EnableWindow(0);if(forcount[27]==0){forcount[27]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton81()
{
		char x=detectmines(3,0);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[28]==0)
			{
				checkifopen[28]=1;
				OnBnClickedButton82();
				OnBnClickedButton66();
				OnBnClickedButton64();
				OnBnClickedButton80();
				OnBnClickedButton89();
			}
		}
		else
	{
		CString pp(x,1);		
		b28.SetWindowTextW(pp);
	}
	}
	else
	{
		b28.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b28.EnableWindow(0);if(forcount[28]==0){forcount[28]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton64()
{
		char x=detectmines(3,1);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[29]==0)
			{
				checkifopen[29]=1;
				OnBnClickedButton82();
				OnBnClickedButton66();
				OnBnClickedButton67();
				OnBnClickedButton65();
				OnBnClickedButton63();
				OnBnClickedButton80();
				OnBnClickedButton89();
				OnBnClickedButton81();
			}
		}
		else
	{
		CString pp(x,1);		
		b29.SetWindowTextW(pp);
	}
	}
	else
	{
		b29.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b29.EnableWindow(0);if(forcount[29]==0){forcount[29]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton65()
{
		char x=detectmines(3,2);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[30]==0)
			{
				checkifopen[30]=1;
				OnBnClickedButton66();
				OnBnClickedButton67();
				OnBnClickedButton48();
				OnBnClickedButton46();
				OnBnClickedButton62();
				OnBnClickedButton63();
				OnBnClickedButton80();
				OnBnClickedButton64();
			}
		}
		else
	{
		CString pp(x,1);		
		b30.SetWindowTextW(pp);
	}
	}
	else
	{
		b30.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b30.EnableWindow(0);if(forcount[30]==0){forcount[30]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton46()
{
		char x=detectmines(3,3);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[31]==0)
			{
				checkifopen[31]=1;
				OnBnClickedButton67();
				OnBnClickedButton48();
				OnBnClickedButton49();
				OnBnClickedButton47();
				OnBnClickedButton45();
				OnBnClickedButton62();
				OnBnClickedButton63();
				OnBnClickedButton65();
			}
		}
		else
	{
		CString pp(x,1);		
		b31.SetWindowTextW(pp);
	}
	}
	else
	{
		b31.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b31.EnableWindow(0);if(forcount[31]==0){forcount[31]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton47()
{
		char x=detectmines(3,4);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[32]==0)
			{
				checkifopen[32]=1;
				OnBnClickedButton48();
				OnBnClickedButton49();
				OnBnClickedButton30();
				OnBnClickedButton28();
				OnBnClickedButton44();
				OnBnClickedButton45();
				OnBnClickedButton62();
				OnBnClickedButton46();
			}
		}
		else
	{
		CString pp(x,1);		
		b32.SetWindowTextW(pp);
	}
	}
	else
	{
		b32.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b32.EnableWindow(0);if(forcount[32]==0){forcount[32]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton28()
{
	char x=detectmines(3,5);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[33]==0)
			{
				checkifopen[33]=1;
				OnBnClickedButton49();
				OnBnClickedButton30();
				OnBnClickedButton31();
				OnBnClickedButton29();
				OnBnClickedButton27();
				OnBnClickedButton44();
				OnBnClickedButton45();
				OnBnClickedButton47();
			}
		}
		else
	{
		CString pp(x,1);		
		b33.SetWindowTextW(pp);
	}
	}
	else
	{
		b33.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b33.EnableWindow(0);if(forcount[33]==0){forcount[33]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton29()
{
	char x=detectmines(3,6);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[34]==0)
			{
				checkifopen[34]=1;
				OnBnClickedButton30();
				OnBnClickedButton31();
				OnBnClickedButton12();
				OnBnClickedButton10();
				OnBnClickedButton26();
				OnBnClickedButton27();
				OnBnClickedButton44();
				OnBnClickedButton28();
			}
		}
		else
	{
		CString pp(x,1);		
		b34.SetWindowTextW(pp);
	}
	}
	else
	{
		b34.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b34.EnableWindow(0);if(forcount[34]==0){forcount[34]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton10()
{
	char x=detectmines(3,7);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[35]==0)
			{
				checkifopen[35]=1;
				OnBnClickedButton31();
				OnBnClickedButton12();
				OnBnClickedButton13();
				OnBnClickedButton11();
				OnBnClickedButton9();
				OnBnClickedButton26();
				OnBnClickedButton27();
				OnBnClickedButton29();
			}
		}
		else
	{
		CString pp(x,1);		
		b35.SetWindowTextW(pp);
	}
	}
	else
	{
		b35.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b35.EnableWindow(0);if(forcount[35]==0){forcount[35]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton11()
{
	char x=detectmines(3,8);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[36]==0)
			{
				checkifopen[36]=1;
				OnBnClickedButton12();
				OnBnClickedButton13();
				OnBnClickedButton10();
				OnBnClickedButton26();
				OnBnClickedButton9();
			}
		}
		else
	{
		CString pp(x,1);		
		b36.SetWindowTextW(pp);
	}
	}
	else
	{
		b36.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b36.EnableWindow(0);if(forcount[36]==0){forcount[36]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton89()
{
	char x=detectmines(4,0);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[37]==0)
			{
				checkifopen[37]=1;
				OnBnClickedButton81();
				OnBnClickedButton64();
				OnBnClickedButton80();
				OnBnClickedButton78();
				OnBnClickedButton88();
			
			}
		}
		else
	{
		CString pp(x,1);		
		b37.SetWindowTextW(pp);
	}
	}
	else
	{
		b37.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b37.EnableWindow(0);if(forcount[37]==0){forcount[37]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton80()
{
	char x=detectmines(4,1);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[38]==0)
			{
				checkifopen[38]=1;
				OnBnClickedButton81();
				OnBnClickedButton64();
				OnBnClickedButton65();
				OnBnClickedButton63();
				OnBnClickedButton79();
				OnBnClickedButton78();
				OnBnClickedButton88();
				OnBnClickedButton89();
			}
		}
		else
	{
		CString pp(x,1);		
		b38.SetWindowTextW(pp);
	}
	}
	else
	{
		b38.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b38.EnableWindow(0);if(forcount[38]==0){forcount[38]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton63()
{
	char x=detectmines(4,2);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[39]==0)
			{
				checkifopen[39]=1;
				OnBnClickedButton64();
				OnBnClickedButton65();
				OnBnClickedButton46();
				OnBnClickedButton62();
				OnBnClickedButton60();
				OnBnClickedButton79();
				OnBnClickedButton78();
				OnBnClickedButton80();
			}
		}
		else
	{
		CString pp(x,1);		
		b39.SetWindowTextW(pp);
	}
	}
	else
	{
		b39.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b39.EnableWindow(0);if(forcount[39]==0){forcount[39]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton62()
{
	char x=detectmines(4,3);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[40]==0)
			{
				checkifopen[40]=1;
				OnBnClickedButton65();
				OnBnClickedButton46();
				OnBnClickedButton47();
				OnBnClickedButton45();
				OnBnClickedButton61();
				OnBnClickedButton60();
				OnBnClickedButton79();
				OnBnClickedButton63();
			}
		}
		else
	{
		CString pp(x,1);		
		b40.SetWindowTextW(pp);
	}
	}
	else
	{
		b40.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b40.EnableWindow(0);if(forcount[40]==0){forcount[40]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton45()
{
	char x=detectmines(4,4);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[41]==0)
			{
				checkifopen[41]=1;
				OnBnClickedButton46();
				OnBnClickedButton47();
				OnBnClickedButton28();
				OnBnClickedButton44();
				OnBnClickedButton42();
				OnBnClickedButton61();
				OnBnClickedButton60();
				OnBnClickedButton62();
			}
		}
		else
	{
		CString pp(x,1);		
		b41.SetWindowTextW(pp);
	}
	}
	else
	{
		b41.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b41.EnableWindow(0);if(forcount[41]==0){forcount[41]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton44()
{
		char x=detectmines(4,5);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[42]==0)
			{
				checkifopen[42]=1;
				OnBnClickedButton47();
				OnBnClickedButton28();
				OnBnClickedButton29();
				OnBnClickedButton27();
				OnBnClickedButton43();
				OnBnClickedButton42();
				OnBnClickedButton61();
				OnBnClickedButton45();
			}
		}
		else
	{
		CString pp(x,1);		
		b42.SetWindowTextW(pp);
	}
	}
	else
	{
		b42.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b42.EnableWindow(0);if(forcount[42]==0){forcount[42]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton27()
{
		char x=detectmines(4,6);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[43]==0)
			{
				checkifopen[43]=1;
				OnBnClickedButton28();
				OnBnClickedButton29();
				OnBnClickedButton10();
				OnBnClickedButton26();
				OnBnClickedButton24();
				OnBnClickedButton43();
				OnBnClickedButton43();
				OnBnClickedButton44();
			}
		}
		else
	{
		CString pp(x,1);		
		b43.SetWindowTextW(pp);
	}
	}
	else
	{
		b43.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b43.EnableWindow(0);if(forcount[43]==0){forcount[43]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton26()
{
	char x=detectmines(4,7);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[44]==0)
			{
				checkifopen[44]=1;
				OnBnClickedButton29();
				OnBnClickedButton10();
				OnBnClickedButton11();
				OnBnClickedButton9();
				OnBnClickedButton25();
				OnBnClickedButton24();
				OnBnClickedButton43();
				OnBnClickedButton27();
			}
		}
		else
	{
		CString pp(x,1);		
		b44.SetWindowTextW(pp);
	}
	}
	else
	{
		b44.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b44.EnableWindow(0);if(forcount[44]==0){forcount[44]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton9()
{
	char x=detectmines(4,8);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[45]==0)
			{
				checkifopen[45]=1;
				OnBnClickedButton10();
				OnBnClickedButton11();
				OnBnClickedButton26();
				OnBnClickedButton24();
				OnBnClickedButton25();
			
			}
		}
		else
	{
		CString pp(x,1);		
		b45.SetWindowTextW(pp);
	}
	}
	else
	{
		b45.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b45.EnableWindow(0);if(forcount[45]==0){forcount[45]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton88()
{
		char x=detectmines(5,0);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[46]==0)
			{
				checkifopen[46]=1;
				OnBnClickedButton89();
				OnBnClickedButton80();
				OnBnClickedButton78();
				OnBnClickedButton76();
				OnBnClickedButton87();
			}
		}
		else
	{
		CString pp(x,1);		
		b46.SetWindowTextW(pp);
	}
	}
	else
	{
		b46.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b46.EnableWindow(0);if(forcount[46]==0){forcount[46]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton78()
{
	char x=detectmines(5,1);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[47]==0)
			{
				checkifopen[47]=1;
				OnBnClickedButton89();
				OnBnClickedButton80();
				OnBnClickedButton63();
				OnBnClickedButton79();
				OnBnClickedButton77();
				OnBnClickedButton76();
				OnBnClickedButton87();
				OnBnClickedButton88();
			}
		}
		else
	{
		CString pp(x,1);		
		b47.SetWindowTextW(pp);
	}
	}
	else
	{
		b47.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b47.EnableWindow(0);if(forcount[47]==0){forcount[47]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton79()
{
	char x=detectmines(5,2);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[48]==0)
			{
				checkifopen[48]=1;
				OnBnClickedButton80();
				OnBnClickedButton63();
				OnBnClickedButton62();
				OnBnClickedButton60();
				OnBnClickedButton58();
				OnBnClickedButton77();
				OnBnClickedButton76();
				OnBnClickedButton78();
			}
		}
		else
	{
		CString pp(x,1);		
		b48.SetWindowTextW(pp);
	}
	}
	else
	{
		b48.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b48.EnableWindow(0);if(forcount[48]==0){forcount[48]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton60()
{
char x=detectmines(5,3);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[49]==0)
			{
				checkifopen[49]=1;
				OnBnClickedButton63();
				OnBnClickedButton62();
				OnBnClickedButton45();
				OnBnClickedButton61();
				OnBnClickedButton59();
				OnBnClickedButton58();
				OnBnClickedButton77();
				OnBnClickedButton79();
			}
		}
		else
	{
		CString pp(x,1);		
		b49.SetWindowTextW(pp);
	}
	}
	else
	{
		b49.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b49.EnableWindow(0);if(forcount[49]==0){forcount[49]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton61()
{
	char x=detectmines(5,4);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[50]==0)
			{
				checkifopen[50]=1;
				OnBnClickedButton62();
				OnBnClickedButton45();
				OnBnClickedButton44();
				OnBnClickedButton42();
				OnBnClickedButton40();
				OnBnClickedButton59();
				OnBnClickedButton58();
				OnBnClickedButton60();
			}
		}
		else
	{
		CString pp(x,1);		
		b50.SetWindowTextW(pp);
	}
	}
	else
	{
		b50.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b50.EnableWindow(0);if(forcount[50]==0){forcount[50]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton42()
{
	char x=detectmines(5,5);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[51]==0)
			{
				checkifopen[51]=1;
				OnBnClickedButton45();
				OnBnClickedButton44();
				OnBnClickedButton27();
				OnBnClickedButton43();
				OnBnClickedButton41();
				OnBnClickedButton40();
				OnBnClickedButton59();
				OnBnClickedButton61();
			}
		}
		else
	{
		CString pp(x,1);		
		b51.SetWindowTextW(pp);
	}
	}
	else
	{
		b51.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b51.EnableWindow(0);if(forcount[51]==0){forcount[51]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton43()
{
	char x=detectmines(5,6);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[52]==0)
			{
				checkifopen[52]=1;
				OnBnClickedButton44();
				OnBnClickedButton27();
				OnBnClickedButton26();
				OnBnClickedButton24();
				OnBnClickedButton22();
				OnBnClickedButton41();
				OnBnClickedButton40();
				OnBnClickedButton42();
			}
		}
		else
	{
		CString pp(x,1);		
		b52.SetWindowTextW(pp);
	}
	}
	else
	{
		b52.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b52.EnableWindow(0);if(forcount[52]==0){forcount[52]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton24()
{	
	char x=detectmines(5,7);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[53]==0)
			{
				checkifopen[53]=1;
				OnBnClickedButton27();
				OnBnClickedButton26();
				OnBnClickedButton9();
				OnBnClickedButton25();
				OnBnClickedButton23();
				OnBnClickedButton22();
				OnBnClickedButton41();
				OnBnClickedButton43();
			}
		}
		else
	{
		CString pp(x,1);		
		b53.SetWindowTextW(pp);
	}
	}
	else
	{
		b53.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b53.EnableWindow(0);if(forcount[53]==0){forcount[53]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton25()
{
	char x=detectmines(5,8);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[54]==0)
			{
				checkifopen[54]=1;
				OnBnClickedButton26();
				OnBnClickedButton9();
				OnBnClickedButton24();
				OnBnClickedButton22();
				OnBnClickedButton23();
				
			}
		}
		else
	{
		CString pp(x,1);		
		b54.SetWindowTextW(pp);
	}
	}
	else
	{
		b54.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b54.EnableWindow(0);if(forcount[54]==0){forcount[54]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton87()
{
	char x=detectmines(6,0);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[55]==0)
			{
				checkifopen[55]=1;
				OnBnClickedButton88();
				OnBnClickedButton78();
				OnBnClickedButton76();
				OnBnClickedButton74();
				OnBnClickedButton86();
			
			}
		}
		else
	{
		CString pp(x,1);		
		b55.SetWindowTextW(pp);
	}
	}
	else
	{
		b55.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b55.EnableWindow(0);if(forcount[55]==0){forcount[55]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton76()
{
	char x=detectmines(6,1);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[56]==0)
			{
				checkifopen[56]=1;
				OnBnClickedButton88();
				OnBnClickedButton78();
				OnBnClickedButton79();
				OnBnClickedButton77();
				OnBnClickedButton75();
				OnBnClickedButton74();
				OnBnClickedButton86();
				OnBnClickedButton87();
			}
		}
		else
	{
		CString pp(x,1);		
		b56.SetWindowTextW(pp);
	}
	}
	else
	{
		b56.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b56.EnableWindow(0);if(forcount[56]==0){forcount[56]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton77()
{
	char x=detectmines(6,2);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[57]==0)
			{
				checkifopen[57]=1;
				OnBnClickedButton78();
				OnBnClickedButton79();
				OnBnClickedButton60();
				OnBnClickedButton58();
				OnBnClickedButton56();
				OnBnClickedButton75();
				OnBnClickedButton74();
				OnBnClickedButton76();
			}
		}
		else
	{
		CString pp(x,1);		
		b57.SetWindowTextW(pp);
	}
	}
	else
	{
		b57.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b57.EnableWindow(0);if(forcount[57]==0){forcount[57]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton58()
{
		char x=detectmines(6,3);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[58]==0)
			{
				checkifopen[58]=1;
				OnBnClickedButton79();
				OnBnClickedButton60();
				OnBnClickedButton61();
				OnBnClickedButton59();
				OnBnClickedButton57();
				OnBnClickedButton56();
				OnBnClickedButton75();
				OnBnClickedButton77();
			}
		}
		else
	{
		CString pp(x,1);		
		b58.SetWindowTextW(pp);
	}
	}
	else
	{
		b58.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b58.EnableWindow(0);if(forcount[58]==0){forcount[58]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton59()
{
		char x=detectmines(6,4);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[59]==0)
			{
				checkifopen[59]=1;
				OnBnClickedButton60();
				OnBnClickedButton61();
				OnBnClickedButton42();
				OnBnClickedButton40();
				OnBnClickedButton38();
				OnBnClickedButton57();
				OnBnClickedButton56();
				OnBnClickedButton58();
			}
		}
		else
	{
		CString pp(x,1);		
		b59.SetWindowTextW(pp);
	}
	}
	else
	{
		b59.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b59.EnableWindow(0);if(forcount[59]==0){forcount[59]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton40()
{
	char x=detectmines(6,5);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[60]==0)
			{
				checkifopen[60]=1;
				OnBnClickedButton61();
				OnBnClickedButton42();
				OnBnClickedButton43();
				OnBnClickedButton41();
				OnBnClickedButton39();
				OnBnClickedButton38();
				OnBnClickedButton57();
				OnBnClickedButton59();
			}
		}
		else
	{
		CString pp(x,1);		
		b60.SetWindowTextW(pp);
	}
	}
	else
	{
		b60.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b60.EnableWindow(0);if(forcount[60]==0){forcount[60]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton41()
{
	char x=detectmines(6,6);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[61]==0)
			{
				checkifopen[61]=1;
				OnBnClickedButton42();
				OnBnClickedButton43();
				OnBnClickedButton24();
				OnBnClickedButton22();
				OnBnClickedButton20();
				OnBnClickedButton39();
				OnBnClickedButton38();
				OnBnClickedButton40();
			}
		}
		else
	{
		CString pp(x,1);		
		b61.SetWindowTextW(pp);
	}
	}
	else
	{
		b61.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b61.EnableWindow(0);if(forcount[61]==0){forcount[61]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton22()
{
	char x=detectmines(6,7);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[62]==0)
			{
				checkifopen[62]=1;
				OnBnClickedButton43();
				OnBnClickedButton24();
				OnBnClickedButton25();
				OnBnClickedButton23();
				OnBnClickedButton21();
				OnBnClickedButton20();
				OnBnClickedButton39();
				OnBnClickedButton41();
			}
		}
		else
	{
		CString pp(x,1);		
		b62.SetWindowTextW(pp);
	}
	}
	else
	{
		b62.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b62.EnableWindow(0);if(forcount[62]==0){forcount[62]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton23()
{
		char x=detectmines(6,8);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[63]==0)
			{
				checkifopen[63]=1;
				OnBnClickedButton24();
				OnBnClickedButton25();
				OnBnClickedButton22();
				OnBnClickedButton20();
				OnBnClickedButton21();
			
			}
		}
		else
	{
		CString pp(x,1);		
		b63.SetWindowTextW(pp);
	}
	}
	else
	{
		b63.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b63.EnableWindow(0);if(forcount[63]==0){forcount[63]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton86()
{
		char x=detectmines(7,0);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[64]==0)
			{
				checkifopen[64]=1;
				OnBnClickedButton87();
				OnBnClickedButton76();
				OnBnClickedButton74();
				OnBnClickedButton72();
				OnBnClickedButton85();
				
			}
		}
		else
	{
		CString pp(x,1);		
		b64.SetWindowTextW(pp);
	}
	}
	else
	{
		b64.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b64.EnableWindow(0);if(forcount[64]==0){forcount[64]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton74()
{
			char x=detectmines(7,1);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[65]==0)
			{
				checkifopen[65]=1;
				OnBnClickedButton87();
				OnBnClickedButton76();
				OnBnClickedButton77();
				OnBnClickedButton75();
				OnBnClickedButton73();
				OnBnClickedButton72();
				OnBnClickedButton85();
				OnBnClickedButton86();
			}
		}
		else
	{
		CString pp(x,1);		
		b65.SetWindowTextW(pp);
	}
	}
	else
	{
		b65.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b65.EnableWindow(0);if(forcount[65]==0){forcount[65]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton75()
{
		char x=detectmines(7,2);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[66]==0)
			{
				checkifopen[66]=1;
				OnBnClickedButton76();
				OnBnClickedButton66();
				OnBnClickedButton58();
				OnBnClickedButton56();
				OnBnClickedButton54();
				OnBnClickedButton73();
				OnBnClickedButton72();
				OnBnClickedButton74();
			}
		}
		else
	{
		CString pp(x,1);		
		b66.SetWindowTextW(pp);
	}
	}
	else
	{
		b66.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b66.EnableWindow(0);if(forcount[66]==0){forcount[66]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton56()
{
		char x=detectmines(7,3);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[67]==0)
			{
				checkifopen[67]=1;
				OnBnClickedButton77();
				OnBnClickedButton58();
				OnBnClickedButton59();
				OnBnClickedButton57();
				OnBnClickedButton55();
				OnBnClickedButton54();
				OnBnClickedButton73();
				OnBnClickedButton75();
			}
		}
		else
	{
		CString pp(x,1);		
		b67.SetWindowTextW(pp);
	}
	}
	else
	{
		b67.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b67.EnableWindow(0);if(forcount[67]==0){forcount[67]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton57()
{
	char x=detectmines(7,4);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[68]==0)
			{
				checkifopen[68]=1;
				OnBnClickedButton58();
				OnBnClickedButton59();
				OnBnClickedButton40();
				OnBnClickedButton38();
				OnBnClickedButton36();
				OnBnClickedButton55();
				OnBnClickedButton54();
				OnBnClickedButton56();
			}
		}
		else
	{
		CString pp(x,1);		
		b68.SetWindowTextW(pp);
	}
	}
	else
	{
		b68.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b68.EnableWindow(0);if(forcount[68]==0){forcount[68]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton38()
{
	char x=detectmines(7,5);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[69]==0)
			{
				checkifopen[69]=1;
				OnBnClickedButton59();
				OnBnClickedButton40();
				OnBnClickedButton41();
				OnBnClickedButton39();
				OnBnClickedButton37();
				OnBnClickedButton36();
				OnBnClickedButton55();
				OnBnClickedButton57();
			}
		}
		else
	{
		CString pp(x,1);		
		b69.SetWindowTextW(pp);
	}
	}
	else
	{
		b69.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b69.EnableWindow(0);if(forcount[69]==0){forcount[69]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton39()
{
	char x=detectmines(7,6);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[70]==0)
			{
				checkifopen[70]=1;
				OnBnClickedButton40();
				OnBnClickedButton41();
				OnBnClickedButton22();
				OnBnClickedButton20();
				OnBnClickedButton18();
				OnBnClickedButton37();
				OnBnClickedButton36();
				OnBnClickedButton38();
			}
		}
		else
	{
		CString pp(x,1);		
		b70.SetWindowTextW(pp);
	}
	}
	else
	{
		b70.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b70.EnableWindow(0);if(forcount[70]==0){forcount[70]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton20()
{
	char x=detectmines(7,7);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[71]==0)
			{
				checkifopen[71]=1;
				OnBnClickedButton41();
				OnBnClickedButton22();
				OnBnClickedButton23();
				OnBnClickedButton21();
				OnBnClickedButton19();
				OnBnClickedButton18();
				OnBnClickedButton37();
				OnBnClickedButton39();
			}
		}
		else
	{
		CString pp(x,1);		
		b71.SetWindowTextW(pp);
	}
	}
	else
	{
		b71.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b71.EnableWindow(0);if(forcount[71]==0){forcount[71]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton21()
{
	char x=detectmines(7,8);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[72]==0)
			{
				checkifopen[72]=1;
				OnBnClickedButton22();
				OnBnClickedButton23();
				OnBnClickedButton20();
				OnBnClickedButton18();
				OnBnClickedButton19();
			}
		}
		else
	{
		CString pp(x,1);		
		b72.SetWindowTextW(pp);
	}
	}
	else
	{
		b72.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b72.EnableWindow(0);if(forcount[72]==0){forcount[72]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton85()
{
	char x=detectmines(8,0);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[73]==0)
			{
				checkifopen[73]=1;
				OnBnClickedButton86();
				OnBnClickedButton74();
				OnBnClickedButton72();
			
			}
		}
		else
	{
		CString pp(x,1);		
		b73.SetWindowTextW(pp);
	}
	}
	else
	{
		b73.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b73.EnableWindow(0);if(forcount[73]==0){forcount[73]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton72()
{
	char x=detectmines(8,1);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[74]==0)
			{
				checkifopen[74]=1;
				OnBnClickedButton85();
				OnBnClickedButton86();
				OnBnClickedButton74();
				OnBnClickedButton75();
				OnBnClickedButton73();
		
			}
		}
		else
	{
		CString pp(x,1);		
		b74.SetWindowTextW(pp);
	}
	}
	else
	{
		b74.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b74.EnableWindow(0);if(forcount[74]==0){forcount[74]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton73()
{
	char x=detectmines(8,2);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[75]==0)
			{
				checkifopen[75]=1;
				OnBnClickedButton74();
				OnBnClickedButton75();
				OnBnClickedButton56();
				OnBnClickedButton54();
				OnBnClickedButton72();
				
			}
		}
		else
	{
		CString pp(x,1);		
		b75.SetWindowTextW(pp);
	}
	}
	else
	{
		b75.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b75.EnableWindow(0);if(forcount[75]==0){forcount[75]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton54()
{
	char x=detectmines(8,3);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[76]==0)
			{
				checkifopen[76]=1;
				OnBnClickedButton73();
				OnBnClickedButton75();
				OnBnClickedButton56();
				OnBnClickedButton57();
				OnBnClickedButton55();
				
			}
		}
		else
	{
		CString pp(x,1);		
		b76.SetWindowTextW(pp);
	}
	}
	else
	{
		b76.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b76.EnableWindow(0);if(forcount[76]==0){forcount[76]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton55()
{
	char x=detectmines(8,4);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[77]==0)
			{
				checkifopen[77]=1;
				OnBnClickedButton54();
				OnBnClickedButton56();
				OnBnClickedButton38();
				OnBnClickedButton57();
				OnBnClickedButton36();
			
			}
		}
		else
	{
		CString pp(x,1);		
		b77.SetWindowTextW(pp);
	}
	}
	else
	{
		b77.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b77.EnableWindow(0);if(forcount[77]==0){forcount[77]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton36()
{
	char x=detectmines(8,5);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[78]==0)
			{
				checkifopen[78]=1;
				OnBnClickedButton55();
				OnBnClickedButton57();
				OnBnClickedButton38();
				OnBnClickedButton39();
				OnBnClickedButton37();
		
			}
		}
		else
	{
		CString pp(x,1);		
		b78.SetWindowTextW(pp);
	}
	}
	else
	{
		b78.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b78.EnableWindow(0);if(forcount[78]==0){forcount[78]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton37()
{
	char x=detectmines(8,6);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[79]==0)
			{
				checkifopen[79]=1;
				OnBnClickedButton36();
				OnBnClickedButton38();
				OnBnClickedButton39();
				OnBnClickedButton20();
				OnBnClickedButton18();
				
			}
		}
		else
	{
		CString pp(x,1);		
		b79.SetWindowTextW(pp);
	}
	}
	else
	{
		b79.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b79.EnableWindow(0);if(forcount[79]==0){forcount[79]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton18()
{
	char x=detectmines(8,7);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[80]==0)
			{
				checkifopen[80]=1;
				OnBnClickedButton37();
				OnBnClickedButton39();
				OnBnClickedButton20();
				OnBnClickedButton21();
				OnBnClickedButton19();
			
			}
		}
		else
	{
		CString pp(x,1);		
		b80.SetWindowTextW(pp);
	}
	}
	else
	{
		b80.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b80.EnableWindow(0);if(forcount[80]==0){forcount[80]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}

void CMinesweeperDlg::OnBnClickedButton19()
{
	char x=detectmines(8,8);
	if(x!='n')
		{
		if(x==' ')
		{
			if(checkifopen[81]==0)
			{
				checkifopen[81]=1;
				OnBnClickedButton18();
				OnBnClickedButton20();
				OnBnClickedButton21();
				
			}
		}
		else
	{
		CString pp(x,1);		
		b81.SetWindowTextW(pp);
	}
	}
	else
	{
		b81.SetBitmap( ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP1)) );
	}
	
	b81.EnableWindow(0);if(forcount[81]==0){checkifopen[81]=1;count++;};if(count==71){GameWonDlg kk; kk.DoModal();}
}



void CMinesweeperDlg::OnBnClickedButton1()
{
	EndDialog(1);

	CMinesweeperDlg Reset;
	Reset.DoModal();
}


void CMinesweeperDlg::OnGameChangeappearence()
{

	// TODO: Add your command handler code here
	OptionDlg godzilla;
	int x=godzilla.DoModal();
	if(x==1)
	{::PostQuitMessage(1);
	}

}



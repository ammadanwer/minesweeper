// ExitDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Minesweeper.h"
#include "ExitDlg.h"


// ExitDlg dialog

IMPLEMENT_DYNAMIC(ExitDlg, CDialog)


ExitDlg::ExitDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ExitDlg::IDD, pParent)
{
	
}

ExitDlg::~ExitDlg()
{
}

void ExitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, static_id, mystatic);
	CFont font;
    font.CreatePointFont(20,L"Georgia",NULL);
     this->GetDlgItem(static_id)->SetFont( &font );
}


BEGIN_MESSAGE_MAP(ExitDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON3, &ExitDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON2, &ExitDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON1, &ExitDlg::dontsave)
END_MESSAGE_MAP()


// ExitDlg message handlers

void ExitDlg::OnBnClickedButton3()
{
	EndDialog(1);
}


void ExitDlg::OnBnClickedButton2()
{
	EndDialog(2);
}

void ExitDlg::dontsave()
{
	EndDialog(3);
}

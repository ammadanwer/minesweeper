#pragma once
#include "afxwin.h"


// ExitDlg dialog

class ExitDlg : public CDialog
{
	DECLARE_DYNAMIC(ExitDlg)

public:
	ExitDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~ExitDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton2();
	afx_msg void dontsave();
	CStatic mystatic;
};

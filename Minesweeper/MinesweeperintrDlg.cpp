// MinesweeperintrDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Minesweeper.h"
#include "MinesweeperintrDlg.h"
#include "MinesweeperDlg.h"


// MinesweeperintrDlg dialog

IMPLEMENT_DYNAMIC(MinesweeperintrDlg, CDialog)

MinesweeperintrDlg::MinesweeperintrDlg(CWnd* pParent /*=NULL*/)
	: CDialog(MinesweeperintrDlg::IDD, pParent)
{
	 AfxGetApp()->LoadIcon(IDI_ICON1);
}

MinesweeperintrDlg::~MinesweeperintrDlg()
{
}

void MinesweeperintrDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(MinesweeperintrDlg, CDialog)
END_MESSAGE_MAP()


// MinesweeperintrDlg message handlers
